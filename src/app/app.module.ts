import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { ProfilComponent } from './component/profil/profil.component';
import { ListProfilComponent } from './component/list-profil/list-profil.component';
import {RouterModule, Routes} from '@angular/router';
import {HttpClientModule} from '@angular/common/http';
import {ZinderApiService} from './service/zinderApi.service';
import { MatchComponent } from './component/match/match.component';
import {FormsModule} from '@angular/forms';
import { StatistiquesComponent } from './component/statistiques/statistiques.component';
import { ListMatchComponent } from './component/list-match/list-match.component';
import { AdminComponent } from './component/admin/admin.component';
import { CreateProfilComponent } from './component/create-profil/create-profil.component';

const appRoutes: Routes = [
  {path : 'stats', component : StatistiquesComponent },
  {path : 'admin', component : AdminComponent },
  {path : '', component : ListProfilComponent }


];

@NgModule({
  declarations: [
    AppComponent,
    ProfilComponent,
    ListProfilComponent,
    MatchComponent,
    StatistiquesComponent,
    ListMatchComponent,
    AdminComponent,
    CreateProfilComponent
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot(
      appRoutes
    ),
    HttpClientModule,
    FormsModule
  ],
  providers: [ZinderApiService],
  bootstrap: [AppComponent]
})
export class AppModule { }
