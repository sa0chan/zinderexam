export class MatchGettedModel {
  id: string;
  match: boolean;
  profil: string;

  constructor(id: string, profil: string, match: boolean) {
    this.match = match;
    this.id = id;
    this.profil = profil;

  }

}
