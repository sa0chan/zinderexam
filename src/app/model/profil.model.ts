export class ProfilModel {

  nom: string;
  prenom: string;
  photoUrl: string;
  interets: string[];
  id: string;

  constructor(id: string, nom: string, prenom: string, photoUrl: string, interets: string[]) {
    this.nom = nom;
    this.prenom = prenom;
    this.photoUrl = photoUrl;
    this.interets = interets;
    this.id = id ;
  }

}

