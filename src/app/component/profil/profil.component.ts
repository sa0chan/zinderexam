import {Component, Input, OnInit} from '@angular/core';
import {ProfilModel} from '../../model/profil.model';
import {InteretModel} from '../../model/interet.model';
import {ZinderApiService} from '../../service/zinderApi.service';

@Component({
  selector: 'app-profil',
  templateUrl: './profil.component.html',
  styleUrls: ['./profil.component.css']
})
export class ProfilComponent implements OnInit {

  @Input() profil: ProfilModel;
  @Input()interetChoiceList: InteretModel[] = [];


  constructor(private zinderService: ZinderApiService) {

  }

  ngOnInit() {
    this.zinderService.getInterets().subscribe(response => {
      response.forEach(interet => {
        this.interetChoiceList.push(interet.nom);
      });
    });

  }


}
