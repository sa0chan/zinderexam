import {ZinderApiService} from '../../service/zinderApi.service';
import {ProfilModel} from '../../model/profil.model';
import {Component, Input, OnInit} from '@angular/core';
import {InteretModel} from '../../model/interet.model';

@Component({
  selector: 'app-list-profil',
  templateUrl: './list-profil.component.html',
  styleUrls: ['./list-profil.component.css']
})
export class ListProfilComponent implements OnInit {

  profilList: ProfilModel[] = [];
  @Input() interetChoiceList: InteretModel[] = [];
  interetSelected: InteretModel;

  constructor(private zinderService: ZinderApiService) {
  }


  ngOnInit() {
    this.zinderService.getAllProfils().subscribe(response => {
      const profils = response.profils;
      profils.forEach(profil => {
        this.profilList.push(profil);
      });

    });

    this.zinderService.getInterets().subscribe(response => {
      response.forEach(interet => {
        this.interetChoiceList.push(interet.nom);
      });
    });


  }


}
