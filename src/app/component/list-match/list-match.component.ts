import {Component, OnInit} from '@angular/core';
import {ZinderApiService} from '../../service/zinderApi.service';
import {MatchGettedModel} from '../../model/matchGetted.model';
import {ProfilModel} from '../../model/profil.model';
import {repeat} from 'rxjs/operators';

@Component({
  selector: 'app-list-match',
  templateUrl: './list-match.component.html',
  styleUrls: ['./list-match.component.css']
})
export class ListMatchComponent implements OnInit {
  listMatches: MatchGettedModel[] = [];
  listProfils: ProfilModel[];
  matchIdProfil: string;
  matchToDelete: string;


  constructor(private service: ZinderApiService) {
  }

  ngOnInit() {

    this.service.getMatches().subscribe(response => {
      this.listMatches = response;
      this.listMatches.forEach(match => {
        this.matchIdProfil = match.profil;

        this.service.getAllProfils().subscribe(response => {
          this.listProfils = response.profils;
          this.listProfils.filter(profil => {
            return profil.id === this.matchIdProfil;
          });
        });
      });

    });
  }

  deleteMatch(id: string) {

    this.listMatches.filter(match => {
      if (match.profil === id) {
        return this.matchToDelete = match.id;
      }
    });
    console.log(this.matchToDelete)
    this.service.deleteMatchById(this.matchToDelete).subscribe(() => {
      this.reloadListMatch();
  });
  }

  reloadListMatch() {

    this.service.getMatches().subscribe(response => {
      console.log(response);
      this.listMatches = response;

    });
  }

}

