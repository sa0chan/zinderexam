import {Component, Input, OnInit} from '@angular/core';
import {ZinderApiService} from '../../service/zinderApi.service';
import {MatchModel} from '../../model/match.model';
import {stringify} from 'querystring';

@Component({
  selector: 'app-match',
  templateUrl: './match.component.html',
  styleUrls: ['./match.component.css']
})
export class MatchComponent implements OnInit {

  @Input() isMatching: boolean;
  @Input() idProfil: string;

  constructor(private zinderService: ZinderApiService) {
  }

  ngOnInit() {
  }

  getChoiceMatch(choice: boolean) {
    this.isMatching = choice;
    const match = new MatchModel(this.isMatching);
    console.log(match);
    this.zinderService.postMatching(this.idProfil, match).subscribe();
  }
}
