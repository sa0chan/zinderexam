import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {ProfilModel} from '../model/profil.model';
import {MatchModel} from '../model/match.model';
import {InteretModel} from '../model/interet.model';


@Injectable()
export class ZinderApiService {

  constructor(private http: HttpClient) {
  }

  // Observable
  getAllProfils(): Observable<ProfilModel[]> {
    return this.http.get<ProfilModel[]>('http://localhost:8088/zinder/profils');
  }


  postMatching(idProfil: string, body: MatchModel): Observable<MatchModel> {
    return this.http.post<MatchModel>(`http://localhost:8088/zinder/profils/${idProfil}/match`, body);
  }

  getMatches(): Observable<MatchModel[]> {
    return this.http.get<MatchModel[]>('http://localhost:8088/zinder/matchs');
  }

  deleteMatchById(id): Observable<MatchModel> {
    return this.http.delete<MatchModel>(`http://localhost:8088/zinder/matchs/${id}`);
  }

  getInterets(): Observable<InteretModel[]> {
    return this.http.get<InteretModel[]>('http://localhost:8088/zinder/interets');
  }


}
